/** @type {import('next').NextConfig} */

module.exports = {
    images: {
        unoptimized: true
    },
    output: 'standalone',
    env: {
        APP_VERSION: process.env.APP_VERSION || "",
    }
}