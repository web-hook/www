export default function Tryit() {
    return (
        <section className="bg-white py-4">
            <div className="container max-w-5xl mx-auto m-8">
                <h1 className="w-full my-2 text-5xl font-bold leading-tight text-center text-gray-800">
                    Try it locally
                </h1>
                <div className="w-full mb-4">
                    <div className="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t"></div>
                </div>
                <div className="flex flex-wrap flex-col-reverse sm:flex-row">
                    <div className="w-full sm:w-1/2 p-6 mt-6">
                        <img className="w-full md:w-5/5 z-50" src="./images/gitlab.png" />
                    </div>
                    <div className="w-full sm:w-1/2 p-6 mt-6">
                        <div className="align-middle">
                            <p className="text-gray-600 mb-8">
                              Explore the codebase on <a href="https://gitlab.com/web-hook" className="hover:text-blue-600 underline transition-colors" target="_blank">GitLab</a> and deploy it on your local cluster.
                            </p>
                            <p className="text-gray-600 mb-8">
                                Whether you have feature suggestions, improvements, or bug reports, please don't hesitate to reach out.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
