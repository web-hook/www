export default function Terms() {
    return (
        <section className="bg-white py-4">
            <div className="container max-w-5xl mx-auto my-4 mb-12">
                <h1 className="w-full my-2 text-5xl font-bold leading-tight text-center text-gray-800">
                    Terms of usage
                </h1>
                <div className="w-full mb-4">
                    <div className="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t"></div>
                </div>
                <div className="flex flex-wrap">
                    <div className="w-6/6 sm:w-2/2 p-6">
                        <p className="text-gray-600 mb-4">
                            This application should be used for tests and demos. It's not a production environment in any way (yet...).
                        </p>
                        <p className="text-gray-600 mb-4">
                            Data are only stored for a couple of days.
                        </p>
                        <p className="text-gray-600 mb-4">
                            Do not send sensitive data to those webhooks.
                        </p>
                    </div>
                </div>
            </div>
        </section>
    )
}
