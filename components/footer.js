import Link from 'next/link'

const APP_VERSION = process.env.APP_VERSION;

export default function Footer() {
  return (
    <footer className="bg-cerise-pink">
      <ul className="pl-2">
        <li className="inline-block mr-2">
          <a className="text-white no-underline text-sm">
          { APP_VERSION }
          </a>
        </li>
        <li className="inline-block mr-2 ">
          <a className="text-white no-underline text-sm" href="mailto:contact@webhooks.app">
            Contact
          </a>
        </li>
        <li className="inline-block float-right mr-2">
          <a href="https://www.freepik.com/free-photos-vectors/background" className="text-white text-sm">Background by freepik.com</a>
        </li>
      </ul>
    </footer>
  )
}
