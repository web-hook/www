export default function UseCases() {
    return (
        <section className="bg-white py-4">
            <div className="container max-w-5xl mx-auto m-8">
                <h1 className="w-full my-2 text-5xl font-bold leading-tight text-center text-gray-800">
                    Sample use cases
                </h1>
                <div className="w-full mb-4">
                    <div className="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t"></div>
                </div>
                <div className="flex flex-wrap flex-col-reverse sm:flex-row">
                    <div className="w-full sm:w-1/2 p-6 mt-6">
                        <img className="w-full md:w-5/5 z-50" src="./images/harbor.png" />
                    </div>
                    <div className="w-full sm:w-1/2 p-6 mt-6">
                        <div className="align-middle">
                            <h3 className="text-3xl text-gray-800 font-bold leading-none mb-3">
                                Easily check the content of your payloads
                            </h3>
                            <p className="text-gray-600 mb-8">
                                Many applications send events using webhooks, which are simple HTTP POST requests. webhooks.app provides a dedicated https
                                endpoint; you just need to configure your application to send data to this endpoint, and check the payloads received.
                            </p>
                            <p className="text-gray-600 mb-8">
                                Example: Harbor registry can send webhooks for many types of events. webhooks.app 
                                allows you to quickly visualize the JSON payload sent for each event.
                            </p>
                        </div>
                    </div>
                </div>
                <div className="w-full mb-4">
                    <div className="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t"></div>
                </div>
                <div className="flex flex-wrap">
                    <div className="w-5/6 sm:w-1/2 p-6">
                        <h3 className="text-3xl text-gray-800 font-bold leading-none mb-3">
                            Send data with an HTTP POST
                        </h3>
                        <p className="text-gray-600 mb-8">
                            webhooks.app is also a simple backend which allows you to send json payload. You can quickly get the
                            history of your data from the swagger UI or with a good old cURL.
                        </p>
                    </div>
                    <div className="w-full sm:w-1/2 p-6">
                        <img className="w-full md:w-5/5 z-50" src="./images/history.png" />
                    </div>
                </div>
                <div className="w-full mb-4">
                    <div className="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t"></div>
                </div>
                <div className="flex flex-wrap flex-col-reverse sm:flex-row">
                    <div className="w-full sm:w-1/2 p-6 mt-6">
                        <img className="w-full md:w-5/5 z-50" src="./images/viz.png" />
                    </div>
                    <div className="w-full sm:w-1/2 p-6 mt-6">
                        <div className="align-middle">
                            <h3 className="text-3xl text-gray-800 font-bold leading-none mb-3">
                                Visualize data
                            </h3>
                            <p className="text-gray-600 mb-8">
                                If the data sent to your webhook contains a numeric field, you can visualize them on a simple linear graphics.
                            </p>
                            <p className="text-gray-600 mb-8">
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
