
import { useState, useEffect } from 'react'

export default function Info(props) {

    const token = props.token
    const [info, setInfo] = useState({})
    const [isTokenVisible, setIsTokenVisible] = useState(false) // State to track visibility of the token

    // Get webhook's info
    useEffect(() => {
        const fetchData = async () => {
            const res = await fetch('/wh/info', {
                headers: { 'Authorization': 'Bearer ' + token }
            })

            const info = await res.json();
            console.log(info)
            setInfo(info)
        }
        fetchData()
    }, []);

    if (Object.keys(info).length === 0) {
        return (
            <div className="pt-24">
                <div className="overflow-hidden bg-white p-1 mx-auto">
                    <div className="rounded-md">
                        <div className="text-center pl-2 text-medium text-gray-500">
                            <p>Webhook not retrieved (yet?)</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    return (
        <div className="pt-24">
            <div className="overflow-hidden bg-white p-1 mx-auto">
                <div className="rounded-md">
                    <div className="text-center pl-2 text-medium text-gray-500">
                        Use the URL <span className="text-blue-500">{window.location.origin}/data</span> and the token <span className="text-blue-500" onClick={() => setIsTokenVisible(!isTokenVisible)} style={{ cursor: 'pointer' }}>{isTokenVisible ? token : '**********'}</span> to connect an application to this webhook
                    </div>
                </div>
            </div>
        </div>
    )
}


