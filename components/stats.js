import { useState, useEffect } from 'react'

export default function Info(props) {

    const [stats, setStats] = useState({})
    const [test, setTest] = useState("");

    // Get webhook's info
    useEffect(() => {
        const fetchData = async () => {
            const res = await fetch('/wh/stats')
            const statistics = await res.json();
            console.log(statistics)
            setStats(statistics)
            setTest(statistics.number_of_webhooks)
        }
        fetchData()
    }, []);

    if (Object.keys(stats).length === 0) {
        return (
            <div className="pt-24">
                <div className="overflow-hidden bg-white p-1 mx-auto">
                    <div className="rounded-md">
                        <div className="text-center pl-2 text-medium text-gray-500">
                            <p>Webhook not retrieved (yet?)</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    return (
        <div className="pt-24">
            <div className="overflow-hidden bg-white p-1 mx-auto">
                <div className="rounded-md">
                    <div className="text-center pl-2 text-medium text-gray-500">
                        <div>
                            <div className="flex">
                                <div className="w-20 text-left uppercase text-sm text-blue-500 font-semibold">URL:</div>
                                <div className="text-black text-sm">{stats.number_of_payloads_received}</div>
                            </div>
                            <div className="flex">
                                <div className="w-20 text-left uppercase text-sm text-blue-500 font-semibold">Token:</div>
                                <div className="text-black text-sm">{stats.number_of_webhooks}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


