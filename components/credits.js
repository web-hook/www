import Image from 'next/image'

export default function Credits(props) {
  const url = props.url
  const link = props.link

  return (
    <div className="bg-white relative w-full flex flex-col sm:flex-row justify-end items-end">
      <a href={link} target="_blank">
        <Image
          src={url}
          width={200}
          height={27}
          className="mb-4 sm:mb-7 mr-4"
          alt="credits"
        />
      </a>        
    </div>
  )
}