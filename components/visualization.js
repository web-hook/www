import { useState, useEffect, useRef } from 'react'
import { Line } from 'react-chartjs-2';
import { Chart, registerables } from 'chart.js';
Chart.register(...registerables);
import * as d3 from 'd3';
import cloud from "d3-cloud";

export default function Visualization(props) {
    const token = props.token;

    const [selectableFields, setSelectableFields] = useState([])
    const [selectedField, setSelectedField] = useState(null)

    const selectableTypes = ["linear", "word cloud"]
    const [selectedType, setSelectedType] = useState("linear")

    const [dataItems, setDataItems] = useState([])

    // Linear graphics
    const [labels, setLabels] = useState([])
    const [values, setValues] = useState([])

    // Word cloud
    const [words, setWords] = useState([])
    const svgRef = useRef(null);

    // Get data
    useEffect(() => {
        const fetchData = async () => {
            const res = await fetch('/data?sort=ascending', {
                headers: { 'Authorization': `Bearer ${token}` }
            })
            const items = await res.json();

            // Check if list of data returned
            if (typeof items === 'object' && items !== null && "data" in items) {
                setDataItems(items.data);
            } else {
                console.error("Invalid 'items' object or no 'data' property found:", items);
            }
        }
        fetchData()
    }, [token]);

    // List of selectable fields
    useEffect(() => {
        if(dataItems.length > 0){

            // Use reduce to accumulate all keys into a Set
            const allKeysSet = dataItems.reduce((keysSet, obj) => {
                Object.keys(obj.content).forEach(key => keysSet.add(key));
                return keysSet;
            }, new Set());
            
            // Convert the Set to an array to get all unique keys
            const allKeysArray = Array.from(allKeysSet);
            setSelectableFields(allKeysArray)
        }
    }, [dataItems]);

    // Parse value into a numeric (if possible)
    function parseFieldValue(item) {
        // Make sure a field was selected
        if(selectedField === null) return null;

        // Get field's value
        if(selectedField in item.content){
            const value = item.content[selectedField];

            // Parse into a numerical
            const numericValue = parseFloat(value);
            if(Number.isNaN(numericValue)) return null;
            return numericValue;
        } else {
            return null;
        }
    }

    // Set linear labels
    function calculateLabels() {
        // Make sure a field was selected
        if(selectedField === null) return null;

        /*
        // Create array with null values
        const arr = new Array(dataItems.length).fill("");

        // Set first and last timestamp
        const tzOffset = new Date().getTimezoneOffset();
        const from = new Date(dataItems[0].created_at) 
        const fromLabel = new Date(from.getTime() - tzOffset*60000).toISOString().substring(0,22).replace("T"," ")
        arr[0] = fromLabel
        const to = new Date(dataItems[dataItems.length - 1].created_at) 
        const toLabel = new Date(to.getTime() - tzOffset*60000).toISOString().substring(0,22).replace("T"," ")
        arr[dataItems.length - 1] = toLabel

        return arr
        */
       return dataItems.map(item => item.created_at.substr(0,19))
    }

    // Get list of words and size for word cloud
    function wordList(){

        const wordOccurrences = dataItems.reduce((acc, obj) => {
            if(selectedField in obj.content){
                let word = obj.content[selectedField]
                acc[word] = (acc[word] || 0) + 1;
            }
            return acc;
        }, {});
        
        const wordCount = Object.keys(wordOccurrences).map(word => ({
            text: word,
            size: wordOccurrences[word]
        }));

        const resultArray = wordCount.map(item => {
            item.size = item.size * 25;
            return item
        })

        return resultArray;
    }

    // Handle labels and values of linear graphic type
    useEffect(() => {
        if(selectedType !== "linear") return;

        if(dataItems.length > 0){
            // Use creation timestamp as label
            //setLabels(dataItems.map(item => item.created_at.substr(0,19)))
            setLabels(calculateLabels())

            // Parse selected field into a numerical value (use null if cannot be parsed)
            setValues(dataItems.map(parseFieldValue))
        }
    }, [dataItems, selectedField, selectedType]);

    // Handle word cloud graphic type
    useEffect(() => {
        if(selectedType !== "word cloud") return;

        if(dataItems.length > 0){
            const words = wordList()
            //TEST: const words = ["one", "one", "one", "one", "two", "three"]
            // .words(words.map((word) => ({ text: word, size: 10 + Math.random() * 40 })))

            // Create the word cloud layout using D3.js
            const width = 800;
            const height = 600;
            const wordCloud = cloud()
            .size([width, height])
            .words(words)
            .padding(5)
            .rotate(() => ~~(Math.random() * 2) * 90)
            .font('Arial')
            .fontSize((d) => d.size)
            .on('end', (cloudWords) => {
                // Render the word cloud using D3.js
                const svg = d3.select(svgRef.current);
                svg.selectAll('*').remove();
                const g = svg.append('g')
                .attr('transform', `translate(${width / 2},${height / 2})`);

                g.selectAll('text')
                .data(cloudWords)
                .enter().append('text')
                .style('font-size', (d) => `${d.size}px`)
                .style('fill', 'steelblue')
                .attr('text-anchor', 'middle')
                .attr('transform', (d) => `translate(${d.x},${d.y})rotate(${d.rotate})`)
                .text((d) => d.text);
            });

            wordCloud.start();
        }
    }, [dataItems, selectedField, selectedType]);

    // CSS styles for the chart container
    const chartContainerStyle = {
        width: '85%', // Adjust the width as needed
        margin: 'auto', // Center the chart horizontally
    };

    const chartData = {
        labels: labels,
        datasets: [
          {
            label: 'Data Visualization',
            data: values,
            fill: true,
            borderColor: 'rgba(75,192,192,1)',
            borderWidth: 2,
          },
        ],
    };
    
    const handleSelectFieldChange = (event) => {
        setSelectedField(event.target.value);
    };

    const handleSelectTypeChange = (event) => {
        setSelectedType(event.target.value);
    };

    return (
        <div className="pt-24">
            <div className="overflow-hidden bg-white p-1 mx-auto">
                <div className="rounded-md">
                    <div className="text-center pl-2 text-medium text-gray-500">
                        <div>
                            <div>
                                Field selected : 
                                <select
                                    value={selectedField}
                                    onChange={handleSelectFieldChange}
                                    style={{ fontSize: '16px' }}
                                >
                                    <option value=""> - none -</option>
                                    {selectableFields.map((item, index) => (
                                    <option key={index} value={item}>
                                        {item}
                                    </option>
                                    ))}
                                </select>
                                {/*}
                                &nbsp;represented as : <select
                                    value={selectedType}
                                    onChange={handleSelectTypeChange}
                                    style={{ fontSize: '16px' }}
                                >
                                    <option value="">Type of representation</option>
                                    {selectableTypes.map((item, index) => (
                                    <option key={index} value={item}>
                                        {item}
                                    </option>
                                    ))}
                                </select>
                                */}
                            </div>
                            <div>
                                (make sure to select a field containing numerical values)
                            </div>
                        </div>
                        { (selectedType == "linear") && 
                        <div style={chartContainerStyle}>
                            <Line data={chartData} />
                        </div>
                        }
                        { (selectedType == "word cloud") && 
                        <div>
                            <svg ref={svgRef} width="100%" height="600" />
                        </div>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}