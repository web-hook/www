import { saveWebhookLocalInfo, getWebhookLocalInfo, createNewWebhook } from './helpers'
import { useState, useEffect } from 'react'
import Link from 'next/link'

export default function Webhook() {

    const [token, setToken] = useState('')
    const [name, setName] = useState('')
    const [isTokenVisible, setIsTokenVisible] = useState(false)

    // Create new webhook
    const createWebhook = async () => {
        console.log("createWebhook")

        // Call API
        const webhookCreationInfo = await createNewWebhook();
        console.log(webhookCreationInfo)

        // Make sure webhook was correctly created
        if (webhookCreationInfo === null) {
            console.log("Error creating webhook")
            return
        }

        // Persist info in local storage
        saveWebhookLocalInfo(webhookCreationInfo["token"], webhookCreationInfo["name"])

        // Update state
        setToken(webhookCreationInfo["token"])
        setName(webhookCreationInfo["name"])
    }

    // Get webhook from local storage or create it if does not exist yet
    useEffect(() => {
        const fetchData = async () => {
            const webhookLocalInfo = await getWebhookLocalInfo()
            console.log(webhookLocalInfo)
            if (webhookLocalInfo != null) {
                setToken(webhookLocalInfo["token"])
                setName(webhookLocalInfo["name"])
            } else {
                await createWebhook();
            }
        }
        console.log("webhook useEffect")
        fetchData()
    }, []);

    return (
        <section className="container mx-auto text-center py-6 mb-6">
            {!token &&
                <>
                    <h1 className="w-full my-2 text-5xl font-bold leading-tight text-center text-white">
                        Check it out
                    </h1>
                    <div className="w-full mb-4">
                        <div className="h-1 mx-auto bg-white w-1/6 opacity-25 my-0 py-0 rounded-t"></div>
                    </div>
                    <h3 className="my-4 text-3xl leading-tight">
                        Get your own secure webhook endpoint right now
                    </h3>
                    <div className="w-full px-8 pt-6 pb-8 my-4">
                        <button
                            className="inline-block py-2 px-4 text-black font-bold no-underline mx-auto lg:mx-0 hover:underline bg-white text-gray-800 font-bold rounded-full my-6 py-4 px-8 shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out"
                            onClick={createWebhook}>
                            Get my webhook
                        </button>
                    </div>
                </>
            }
            {token &&
                <>
                    <div className="my-0 text-white leading-tight">
                        <p className="mx-auto sm:w-1/2 text-sm text-white-800 overflow-auto">
                           Connect an application to your webhook and <Link href="/dashboard" className="inline-block text-white underline hover:text-gray-800">visualize</Link> the payloads received 
                        </p>
                    </div>
                    <div className="my-0 text-white leading-tight">
                    </div>
                    <div className="mx-auto sm:w-1/2 bg-white rounded-md p-4 mt-6 mb-2 flex justify-between">
                        <div>
                            <div className="flex">
                                <div className="w-20 text-left uppercase text-sm text-blue-500 font-semibold">URL:</div>
                                <div className="text-black text-sm">{window.location.href}data</div>
                            </div>
                            <div className="flex">
                                <div className="w-20 text-left uppercase text-sm text-blue-500 font-semibold">Token:</div>
                                <div className="text-black text-sm">
                                    <span onClick={() => setIsTokenVisible(!isTokenVisible)} style={{ cursor: 'pointer' }}>
                                        {isTokenVisible ? token : '**********'}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="hidden"
                            onClick={() => console.log("need to copy that to clickboard")}>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="text-black w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M15.75 17.25v3.375c0 .621-.504 1.125-1.125 1.125h-9.75a1.125 1.125 0 01-1.125-1.125V7.875c0-.621.504-1.125 1.125-1.125H6.75a9.06 9.06 0 011.5.124m7.5 10.376h3.375c.621 0 1.125-.504 1.125-1.125V11.25c0-4.46-3.243-8.161-7.5-8.876a9.06 9.06 0 00-1.5-.124H9.375c-.621 0-1.125.504-1.125 1.125v3.5m7.5 10.375H9.375a1.125 1.125 0 01-1.125-1.125v-9.25m12 6.625v-1.875a3.375 3.375 0 00-3.375-3.375h-1.5a1.125 1.125 0 01-1.125-1.125v-1.5a3.375 3.375 0 00-3.375-3.375H9.75" />
                            </svg>
                        </div>
                    </div>
                </>
            }
        </section >
    )
}