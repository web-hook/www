import { useState, useEffect, useMemo } from 'react';
import JsonView from 'react18-json-view';
import 'react18-json-view/src/style.css'
import useWs from '../compositions/useWs';
import useApi from '../compositions/useApi';

export default function RealtimeMessages ({ token }) {
  const tzOffset = new Date().getTimezoneOffset();
  const [_messageList, setMessages] = useState([]);

    // Handle data related messages considering it is in JSON
  const onMessages = (newMessageList) => setMessages((currMessageList) => [
    ...newMessageList,
    ...currMessageList,
  ]);

  // fix display of `created_at` and `content`
  const messageList = useMemo(
    () => _messageList.map((msg) => ({
      ...msg,
      created_at: new Date(msg.created_at),
      identifier: msg.created_at.substring(0, 26).replace(/\./g,"").replace(/\-/g,"").replace(/\:/g,""),
      content: msg.content,
    })),
    [_messageList],
  );

  // Get webhook's info
  useEffect(() => {
    console.log("token changed to " + token);
    setMessages([]);
  }, [token]);

  useWs({
    onMessages,
    token,
  });

  useApi({
    onMessages,
    token,
  });

  return (
    <div className="bg-white border-b py-2">
      <div className="container mx-auto m-2">
        <ul>
          {messageList.map((msg) => (
            <li key={msg.identifier}>
              <div className="divContainer bg-white mt-2 mb-2">
                <div className="divTimestamp pl-2 uppercase text-blue-500">
                  {new Date(msg.created_at.getTime() - tzOffset*60000).toISOString().substring(0,22).replace("T"," ")}
                </div>

                <div className="divContent pl-2 text-black border">
                  <pre className="preContent overflow-auto">
                    <JsonView src={msg.content} collapsed={3} enableClipboard={false}/>
                  </pre>
                </div>
              </div>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

