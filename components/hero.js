export default function Hero() {
    return (
        <div className="pt-24">
                <div className="w-full justify-center text-center">
                    <h2 className="my-2 mx-4 text-3xl xl:text-4xl font-bold leading-tight">
                    Instant HTTPS POST endpoints for your demos & side projects
                    </h2>
                </div>
        </div>
    )
}