import { useState, useEffect } from 'react'
import Script from 'next/script'
import Header from '../components/header'
import Footer from '../components/footer'

const StatsDashboard = () => {
  const [stats, setStats] = useState({
    number_of_payloads_received: 0,
    number_of_webhooks: 0
  })
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    const fetchStats = async () => {
      try {
        const res = await fetch('/stats')
        const statistics = await res.json()
        console.log(statistics);
        setStats(statistics.stats);
      } catch (error) {
        console.error('Failed to fetch stats:', error)
      } finally {
        setIsLoading(false)
      }
    }
    fetchStats()
  }, [])

  return (
    <div className="flex flex-col h-screen overflow-hidden">
      <Script src="https://www.googletagmanager.com/gtag/js?id=G-F39FHRG5VW" />
      <Script src="/static/js/ga.js" />
      <Script src="/static/js/nav.js" />
      
      <header className="w-full text-center py-8">
        <Header />
      </header>

      <main className="flex-1 overflow-y-auto">
        <div className="max-w-4xl mx-auto px-4 py-8">          
          {isLoading ? (
            <div className="text-center text-gray-500">Loading statistics...</div>
          ) : (
            <div className="grid grid-cols-1 md:grid-cols-2 gap-6">
              <div className="bg-white rounded-lg shadow p-6">
                <h2 className="text-lg font-semibold text-blue-500 mb-2">Webhooks created</h2>
                <p className="text-3xl text-blue-500">{stats.number_of_webhooks}</p>
              </div>
              
              <div className="bg-white rounded-lg shadow p-6">
                <h2 className="text-lg font-semibold text-blue-500 mb-2">Last Webhook created at</h2>
                <p className="text-3xl text-blue-500">{stats.webhooks_last_created_at}</p>
              </div>

              <div className="bg-white rounded-lg shadow p-6">
                <h2 className="text-lg font-semibold text-blue-500 mb-2">Payloads received</h2>
                <p className="text-3xl text-blue-500">{stats.number_of_payloads_received}</p>
              </div>

              <div className="bg-white rounded-lg shadow p-6">
                <h2 className="text-lg font-semibold text-blue-500 mb-2">Last payload received at</h2>
                <p className="text-3xl text-blue-500">{stats.payload_last_received}</p>
              </div>
            </div>
          )}
        </div>
      </main>

      <Footer />
    </div>
  )
}

export default StatsDashboard