import { useState, useEffect } from 'react'
import Script from 'next/script'
import { getWebhookLocalInfo, saveWebhookLocalInfo, getWebhookRemoteInfo } from '../components/helpers'
import Header from '../components/header'
import Visualization from '../components/visualization'

export default function VisualizationDashboard() {
  const [token, setToken] = useState(null)

  // Get webhook info
  const getWebhook = async (whToken) => {

    // Do not go any further if token is empty
    if ((whToken === undefined) || (whToken === '')) {
      console.log("Token is empty")
      alert("Token must not be empty")
      return
    }

    // Check if webhook exists in database
    const webhookRemoteInfo = await getWebhookRemoteInfo(whToken)
    console.log("webhookRemoteInfo:")
    console.log(webhookRemoteInfo)
    if (webhookRemoteInfo != null) {
      console.log("webhook exists")
      console.log(webhookRemoteInfo["info"]["name"]);
      saveWebhookLocalInfo(whToken, webhookRemoteInfo["info"]["name"])
      setToken(whToken)
    } else {
      console.log("webhook does NOT exists")
    }
  }

  // Check if token present in local storage
  useEffect(() => {
    const fetchData = async () => {
      const webhookLocalInfo = await getWebhookLocalInfo()
      if (webhookLocalInfo != null) {
        console.log("Local info")
        console.log(webhookLocalInfo)
        await getWebhook(webhookLocalInfo["token"])
      }
    }
    fetchData()
  }, [token]);

  return (
    <>
    <div className="flex flex-col h-screen overflow-hidden">
      <Script src="https://www.googletagmanager.com/gtag/js?id=G-F39FHRG5VW" />
      <Script src="/static/js/ga.js" />
      <Script src="/static/js/nav.js" />
      <header className="w-full text-center p-1">
        <Header />
      </header>
      <main className="flex-1 overflow-y-scroll">
        <div className="">
            <Visualization token={token}/>
        </div>
      </main>
    </div>
  </>
  )
}
