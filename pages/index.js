import { useState, useEffect } from 'react'
import Script from 'next/script'
import Header from '../components/header'
import Hero from '../components/hero'
import Webhook from '../components/webhook'
import Waves from '../components/waves'
import UseCases from '../components/useCases'
import Tryit from '../components/tryit'
import Terms from '../components/terms'
import Footer from '../components/footer'
import Credits from '../components/credits'

export default function Home() {
  const [config, setConfig] = useState({
    creditsImageURL: "",
    creditsLink: ""
  });
  
  useEffect(() => {
    fetch('/conf.json')
      .then(response => response.json())
      .then(data => {
        console.log('Loaded config:', data);
        setConfig(data);
      })
      .catch(error => {
        console.error('Error loading config:', error);
      });
  }, []);

  return (
    <>
      <Script src="https://www.googletagmanager.com/gtag/js?id=G-F39FHRG5VW" />
      <Script src="/static/js/ga.js" />
      <Script src="/static/js/nav.js" />
      <Header showDashboard={true} />
      <Hero />
      <Webhook />
      <Waves />
      <UseCases />
      <Tryit />
      <Terms />
      {config.creditsImageURL && <Credits url={config.creditsImageURL} link={config.creditsLink} />}
      <Footer />
    </>
  )
}